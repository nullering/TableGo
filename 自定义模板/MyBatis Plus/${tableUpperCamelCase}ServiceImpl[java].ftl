<#-- 生成Service接口实现的自定义模板 -->
<#if jsonParam.searchFeilds?has_content && (jsonParam.searchFeilds["${tableInfo.upperTableName}"]?has_content || jsonParam.searchFeilds["${tableInfo.originalTableName}"]?has_content)>
    <#-- 初始化表的查询字段 -->
    <#assign searchFeilds = jsonParam.searchFeilds["${tableInfo.upperTableName}"] /><#if !searchFeilds?has_content><#assign searchFeilds = jsonParam.searchFeilds["${tableInfo.originalTableName}"] /></#if>
</#if>
<#if jsonParam.joinTables?has_content>
    <#list jsonParam.joinTables as tableName>
        <#-- 判断是否是需要多表关联查询的表 -->
        <#if StringUtils.equalsIgnoreCase(tableInfo.tableName, tableName)>
            <#assign isJoinTable = true />
            <#break>
        </#if>
    </#list>
</#if>
package ${jsonParam.packagePath}

import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

<#if !isJoinTable??>
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
</#if>
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
<#assign importDateUtil = false />
<#if searchFeilds?has_content>
    <#list tableInfo.fieldInfos as fieldInfo>
        <#list searchFeilds as fieldName>
            <#if !importDateUtil && StringUtils.equalsIgnoreCase(fieldInfo.colName, fieldName)>
                <#if fieldInfo.javaType == "Date">
import cn.hutool.core.date.DateUtil;
                    <#assign importDateUtil = true />
                    <#break>
                </#if>
            </#if>
        </#list>
    </#list>
</#if>

import ${jsonParam.basePackagePath}.model.<#if jsonParam.moduleName??>${jsonParam.moduleName}.</#if>${tableInfo.upperCamelCase};
import ${jsonParam.basePackagePath}.model.condition.<#if jsonParam.moduleName??>${jsonParam.moduleName}.</#if>${tableInfo.upperCamelCase}Condition;
import ${jsonParam.basePackagePath}.mapper.<#if jsonParam.moduleName??>${jsonParam.moduleName}.</#if>${tableInfo.upperCamelCase}Mapper;
import ${jsonParam.basePackagePath}.service.<#if jsonParam.moduleName??>${jsonParam.moduleName}.</#if>${tableInfo.upperCamelCase}Service;

/**
 * ${tableInfo.simpleRemark}Service接口实现
 *
 * @author ${paramConfig.author}
 * @version 1.0.0 ${today}
 */
@Service
@Transactional(readOnly = true)
public class ${tableInfo.upperCamelCase}ServiceImpl extends ServiceImpl<${tableInfo.upperCamelCase}Mapper, ${tableInfo.upperCamelCase}> implements ${tableInfo.upperCamelCase}Service {
    private static final Logger logger = LoggerFactory.getLogger(${tableInfo.upperCamelCase}Service.class);

    @Override
    public IPage<${tableInfo.upperCamelCase}> find${tableInfo.upperCamelCase}ByCondition(${tableInfo.upperCamelCase}Condition condition) {
        IPage<${tableInfo.upperCamelCase}> page = condition.buildPage();
<#if isJoinTable?? && isJoinTable>
    <#if searchFeilds?has_content>

        <#list tableInfo.fieldInfos as fieldInfo>
            <#list searchFeilds as fieldName>
                <#if StringUtils.equalsIgnoreCase(fieldInfo.colName, fieldName)>
                    <#if fieldInfo.javaType == "Date">
        if (condition.get${fieldInfo.upperCamelCase}End() != null) {
            condition.set${fieldInfo.upperCamelCase}End(DateUtil.endOfDay(condition.get${fieldInfo.upperCamelCase}End()));
        }
                    </#if>
                </#if>
            </#list>
        </#list>
    </#if>
        return baseMapper.list${tableInfo.upperCamelCase}Page(page, condition);
<#else>
        QueryWrapper<${tableInfo.upperCamelCase}> queryWrapper = condition.buildQueryWrapper(${tableInfo.upperCamelCase}.class);
    <#if searchFeilds?has_content>

        <#list tableInfo.fieldInfos as fieldInfo>
            <#list searchFeilds as fieldName>
                <#if StringUtils.equalsIgnoreCase(fieldInfo.colName, fieldName)>
                    <#if fieldInfo.javaType == "Date">
        if (condition.get${fieldInfo.upperCamelCase}Begin() != null) {
            queryWrapper.lambda().ge(${tableInfo.upperCamelCase}::get${fieldInfo.upperCamelCase}, condition.get${fieldInfo.upperCamelCase}Begin());
        }
        if (condition.get${fieldInfo.upperCamelCase}End() != null) {
            queryWrapper.lambda().le(${tableInfo.upperCamelCase}::get${fieldInfo.upperCamelCase}, DateUtil.endOfDay(condition.get${fieldInfo.upperCamelCase}End()));
        }
                    </#if>
                </#if>
            </#list>
        </#list>
    </#if>
        return this.page(page, queryWrapper);
</#if>
    }

    @Override
    public ${tableInfo.upperCamelCase} get${tableInfo.upperCamelCase}ById(${tableInfo.pkJavaType} ${tableInfo.pkLowerCamelName}) {
        return this.getById(${tableInfo.pkLowerCamelName});
    }

    @Transactional
    @Override
    public boolean add${tableInfo.upperCamelCase}(${tableInfo.upperCamelCase} ${tableInfo.lowerCamelCase}) {
        return this.save(${tableInfo.lowerCamelCase});
    }

    @Transactional
    @Override
    public boolean update${tableInfo.upperCamelCase}(${tableInfo.upperCamelCase} ${tableInfo.lowerCamelCase}) {
        return this.updateById(${tableInfo.lowerCamelCase});
    }

    @Transactional
    @Override
    public boolean delete${tableInfo.upperCamelCase}ById(${tableInfo.pkJavaType} ${tableInfo.pkLowerCamelName}) {
        return this.removeById(${tableInfo.pkLowerCamelName});
    }

    @Transactional
    @Override
    public boolean delete${tableInfo.upperCamelCase}ByIds(List<${tableInfo.pkJavaType}> idList) {
        return this.removeByIds(idList);
    }
}