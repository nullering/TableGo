<#-- 用于生成JavaBean的自定义模板 -->
<#if jsonParam.likeFeilds?has_content && (jsonParam.likeFeilds["${tableInfo.upperTableName}"]?has_content || jsonParam.likeFeilds["${tableInfo.originalTableName}"]?has_content)>
    <#-- 初始化表的模糊查询字段 -->
    <#assign likeFeilds = jsonParam.likeFeilds["${tableInfo.upperTableName}"] /><#if !likeFeilds?has_content><#assign likeFeilds = jsonParam.likeFeilds["${tableInfo.originalTableName}"] /></#if>
</#if>
package ${jsonParam.packagePath}

<#assign importDate = false />
<#assign importBigDecimal = false />
<#list tableInfo.fieldInfos as fieldInfo>
    <#if !importDate && fieldInfo.javaType == "Date">
        <#assign importDate = true />
    <#elseif !importBigDecimal && fieldInfo.javaType == "BigDecimal">
        <#assign importBigDecimal = true />
    </#if>
</#list>
<#if importDate>
import java.util.Date;
import cn.hutool.core.date.DatePattern;
import com.fasterxml.jackson.annotation.JsonFormat;
</#if>
<#if importBigDecimal>
import java.math.BigDecimal;
</#if>
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
<#assign importSqlCondition = false />
<#if likeFeilds?has_content>
    <#list tableInfo.fieldInfos as fieldInfo>
        <#list likeFeilds as fieldName>
                <#if !importSqlCondition && StringUtils.equalsIgnoreCase(fieldInfo.colName, fieldName)>
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.SqlCondition;
                <#assign importSqlCondition = true />
                <#break>
            </#if>
        </#list>
        <#if importSqlCondition><#break></#if>
    </#list>
</#if>
import ${jsonParam.basePackagePath}.common.BaseBean;

/**
 * <#if StringUtils.isNotBlank(tableInfo.remark)>${tableInfo.remark}(${tableInfo.tableName})<#else>${tableInfo.tableName}</#if>
 *
* @author ${paramConfig.author}
* @version 1.0.0 ${today}
 */
@ApiModel(description = "${tableInfo.simpleRemark!tableInfo.tableName}")
@TableName("${tableInfo.tableName}")
public class ${tableInfo.upperCamelCase} extends BaseBean {
    /** 版本号 */
    private static final long serialVersionUID = ${tableInfo.serialVersionUID!'1'}L;
<#if tableInfo.fieldInfos?has_content>
    <#if paramConfig.fileUpdateMode == 0 || paramConfig.fileUpdateMode == 1>

    /* ${String.format(paramConfig.mergeFileMarkBegin, 1)} */
    </#if>
    <#list tableInfo.fieldInfos as fieldInfo>

    @ApiModelProperty(value = "${fieldInfo.remark}")
    <#if fieldInfo.primaryKey>
    @TableId
    <#elseif fieldInfo.javaType == "Date">
    @JsonFormat(timezone = "GMT+8", pattern = DatePattern.NORM_DATETIME_PATTERN)
    </#if>
    <#if likeFeilds?has_content>
        <#list likeFeilds as fieldName>
            <#if StringUtils.equalsIgnoreCase(fieldInfo.colName, fieldName)>
    @TableField(condition = SqlCondition.LIKE)
                <#break>
            </#if>
        </#list>
    </#if>
    private ${fieldInfo.javaType} ${fieldInfo.proName};
    </#list>
    <#if paramConfig.fileUpdateMode == 0 || paramConfig.fileUpdateMode == 1>

    /* ${String.format(paramConfig.mergeFileMarkEnd, 1)} */
    </#if>
    <#if paramConfig.fileUpdateMode == 0 || paramConfig.fileUpdateMode == 1>

    /* ${String.format(paramConfig.mergeFileMarkBegin, 2)} */
    </#if>
    <#list tableInfo.fieldInfos as fieldInfo>

    <#if paramConfig.buildFieldRemark == 0>
    /**
     * 获取${fieldInfo.remark!fieldInfo.proName}
     * 
     * @return ${fieldInfo.simpleRemark!fieldInfo.proName}
     */
    </#if>
    public ${fieldInfo.javaType} get${fieldInfo.upperCamelCase}() {
        return this.${fieldInfo.proName};
    }

    <#if paramConfig.buildFieldRemark == 0>
    /**
     * 设置${fieldInfo.remark!fieldInfo.proName}
     * 
     * @param ${fieldInfo.proName}
<#if StringUtils.isNotBlank(fieldInfo.simpleRemark)>
     *          ${fieldInfo.simpleRemark}
</#if>
     */
    </#if>
    public void set${fieldInfo.upperCamelCase}(${fieldInfo.javaType} ${fieldInfo.proName}) {
        this.${fieldInfo.proName} = ${fieldInfo.proName};
    }
    </#list>
    <#if paramConfig.fileUpdateMode == 0 || paramConfig.fileUpdateMode == 1>

    /* ${String.format(paramConfig.mergeFileMarkEnd, 2)} */
    </#if>
</#if>
}