server:
  port: 8080
  servlet:
    context-path: /${jsonParam.artifactId}
spring:
  profiles:
    active: dev
  application:
    name: ${jsonParam.artifactId}
  main:
    allow-bean-definition-overriding: true
  mvc:
    date-format: yyyy-MM-dd
  jackson:
    date-format: yyyy-MM-dd HH:mm:ss
    locale: zh
    time-zone: GMT+8
    default-property-inclusion: non_null
  datasource:
    type: com.zaxxer.hikari.HikariDataSource
    driver-class-name: com.mysql.jdbc.Driver
    url: jdbc:mysql://127.0.0.1:3306/demo?serverTimezone=Asia/Shanghai&characterEncoding=utf8&useUnicode=true&useSSL=false&nullNamePatternMatchesAll=true&allowPublicKeyRetrieval=true
    username: root
    password: root
    hikari:
      minimum-idle: 2
      maximum-pool-size: 10
      idle-timeout: 30000
      max-lifetime: 1800000
      connection-timeout: 30000
      connection-test-query: SELECT 1
mybatis-plus:
  mapper-locations: classpath*:mapper/**/*Mapper.xml
  typeAliasesPackage: ${jsonParam.packagePath}
  global-config:
    db-config:
      id-type: assign_id
      logic-delete-value: 0
      logic-not-delete-value: 1
  configuration:
    map-underscore-to-camel-case: true
    cache-enabled: false
    log-impl: org.apache.ibatis.logging.stdout.StdOutImpl
swagger:
  title: ${"$"}{spring.application.name}
  description: "@project.description@API文档"
  version: 1.0.0
  enabled: true
  contact:
    name: bianj
    email: tablego@qq.com
    url: http://www.tablego.cn
  base-package: ${jsonParam.groupId}.${jsonParam.artifactIdLowerCase}.controller