<#-- 用于生成Mapper.xml配置的自定义模板 -->
<#if jsonParam.searchFeilds?has_content && (jsonParam.searchFeilds["${tableInfo.upperTableName}"]?has_content || jsonParam.searchFeilds["${tableInfo.originalTableName}"]?has_content)>
    <#-- 初始化表的查询字段 -->
    <#assign searchFeilds = jsonParam.searchFeilds["${tableInfo.upperTableName}"] /><#if !searchFeilds?has_content><#assign searchFeilds = jsonParam.searchFeilds["${tableInfo.originalTableName}"] /></#if>
</#if>
<#if jsonParam.joinTables?has_content>
    <#list jsonParam.joinTables as tableName>
        <#-- 判断是否是需要多表关联查询的表 -->
        <#if StringUtils.equalsIgnoreCase(tableInfo.tableName, tableName)>
            <#assign isJoinTable = true />
            <#break>
        </#if>
    </#list>
</#if>
<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE mapper PUBLIC "-//mybatis.org//DTD Mapper 3.0//EN" "http://mybatis.org/dtd/mybatis-3-mapper.dtd">

<!-- <#if StringUtils.isNotBlank(tableInfo.simpleRemark)>${tableInfo.simpleRemark}(${tableInfo.tableName})<#else>${tableInfo.tableName}</#if> -->
<mapper namespace="${jsonParam.groupId}.${jsonParam.artifactIdLowerCase}.mapper.<#if jsonParam.moduleName??>${jsonParam.moduleName}.</#if>${tableInfo.upperCamelCase}Mapper">
<#if isJoinTable?? && isJoinTable>
    <!-- 字段映射 -->
    <resultMap id="${tableInfo.lowerCamelCase}Map" type="${jsonParam.groupId}.${jsonParam.artifactIdLowerCase}.model.<#if jsonParam.moduleName??>${jsonParam.moduleName}.</#if>${tableInfo.upperCamelCase}"/>

    <#if paramConfig.fileUpdateMode == 0 || paramConfig.fileUpdateMode == 1>
    <!-- ${String.format(paramConfig.mergeFileMarkBegin, 1)} -->
    </#if>
    <!-- 表所有字段 -->
    <sql id="allColumns">
        <#list tableInfo.pagingFieldInfos as pagingList>
        <#list pagingList as fieldInfo><#if StringUtils.isNotBlank(tableInfo.tableAlias)>${tableInfo.tableAlias}.</#if>${fieldInfo.colName}<#if fieldInfo_has_next>, </#if></#list><#if pagingList_has_next>, </#if>
        </#list>
    </sql>
    <#if paramConfig.fileUpdateMode == 0 || paramConfig.fileUpdateMode == 1>
    <!-- ${String.format(paramConfig.mergeFileMarkEnd, 1)} -->
    </#if>

    <!-- 根据查询参数分页查询${tableInfo.simpleRemark} -->
    <select id="list${tableInfo.upperCamelCase}Page" resultMap="${tableInfo.lowerCamelCase}Map">
        SELECT
            <include refid="allColumns" />
        FROM ${tableInfo.tableName} <#if StringUtils.isNotBlank(tableInfo.tableAlias)>${tableInfo.tableAlias} </#if>WHERE <#if StringUtils.isNotBlank(tableInfo.tableAlias)>${tableInfo.tableAlias}.</#if>DELETE_FLAG = 1
    <#if searchFeilds?has_content>
        <#list tableInfo.fieldInfos as fieldInfo>
            <#list searchFeilds as fieldName>
                <#if StringUtils.equalsIgnoreCase(fieldInfo.colName, fieldName)>
                    <#if fieldInfo.javaType == "Date">
        <if test="condition.${fieldInfo.proName}Begin != null">
            AND <#if StringUtils.isNotBlank(tableInfo.tableAlias)>${tableInfo.tableAlias}.</#if>${fieldInfo.colName} &gt;= ${"#"}{condition.${fieldInfo.proName}Begin}
        </if>
        <if test="condition.${fieldInfo.proName}End != null">
            AND <#if StringUtils.isNotBlank(tableInfo.tableAlias)>${tableInfo.tableAlias}.</#if>${fieldInfo.colName} &lt;= ${"#"}{condition.${fieldInfo.proName}End}
        </if>
                    <#else>
        <if test="condition.${fieldInfo.proName} != null<#if fieldInfo.javaType == "String"> and condition.${fieldInfo.proName} != ''</#if>">
            AND <#if StringUtils.isNotBlank(tableInfo.tableAlias)>${tableInfo.tableAlias}.</#if>${fieldInfo.colName}<#if fieldInfo.javaType == "String" && fieldInfo.lowerColName?index_of("_id") == -1 && !fieldInfo.isDictType> LIKE CONCAT('%', ${"#"}{condition.${fieldInfo.proName}}, '%')<#else> = ${"#"}{condition.${fieldInfo.proName}}</#if>
        </if>
                    </#if>
                </#if>
            </#list>
        </#list>
    </#if>
    </select>
</#if>

</mapper>