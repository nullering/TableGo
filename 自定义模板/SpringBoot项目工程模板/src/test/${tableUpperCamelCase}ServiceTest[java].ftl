package ${jsonParam.packagePath}

import java.time.Duration;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.baomidou.mybatisplus.core.metadata.IPage;
import ${jsonParam.groupId}.${jsonParam.artifactIdLowerCase}.model.<#if jsonParam.moduleName??>${jsonParam.moduleName}.</#if>${tableInfo.upperCamelCase};
import ${jsonParam.groupId}.${jsonParam.artifactIdLowerCase}.model.condition.<#if jsonParam.moduleName??>${jsonParam.moduleName}.</#if>${tableInfo.upperCamelCase}Condition;
import ${jsonParam.groupId}.${jsonParam.artifactIdLowerCase}.service.<#if jsonParam.moduleName??>${jsonParam.moduleName}.</#if>${tableInfo.upperCamelCase}Service;

/**
 * ${tableInfo.simpleRemark}Service接口测试
 *
* @author ${paramConfig.author}
* @version 1.0.0 ${today}
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class ${tableInfo.upperCamelCase}ServiceTest {
    private static final Logger logger = LoggerFactory.getLogger(${tableInfo.upperCamelCase}ServiceTest.class);

    @Autowired
    private ${tableInfo.upperCamelCase}Service ${tableInfo.lowerCamelCase}Service;

    /** 测试根据查询参数分页查询${tableInfo.simpleRemark}列表 */
    @Test
    public void testFind${tableInfo.upperCamelCase}ByCondition() {
        Instant begin = Instant.now();

        ${tableInfo.upperCamelCase}Condition condition = new ${tableInfo.upperCamelCase}Condition();
        IPage<${tableInfo.upperCamelCase}> page = ${tableInfo.lowerCamelCase}Service.find${tableInfo.upperCamelCase}ByCondition(condition);
        System.out.println("Records: " + page.getRecords());

        Instant end = Instant.now();
        System.out.println("代码执行消耗时间: " + Duration.between(begin, end).toMillis() + " 毫秒");
    }

    /** 测试根据${tableInfo.simpleRemark}ID查询${tableInfo.simpleRemark} */
    @Test
    public void testGet${tableInfo.upperCamelCase}ById() {
        Instant begin = Instant.now();

        ${tableInfo.upperCamelCase} ${tableInfo.lowerCamelCase} = ${tableInfo.lowerCamelCase}Service.get${tableInfo.upperCamelCase}ById("1241271018726662146");
        System.out.println("${tableInfo.upperCamelCase}: " + ${tableInfo.lowerCamelCase});

        Instant end = Instant.now();
        System.out.println("代码执行消耗时间: " + Duration.between(begin, end).toMillis() + " 毫秒");
    }

    /** 测试新增${tableInfo.simpleRemark} */
    @Test
    public void testAdd${tableInfo.upperCamelCase}() {
        Instant begin = Instant.now();

        ${tableInfo.upperCamelCase} ${tableInfo.lowerCamelCase} = new ${tableInfo.upperCamelCase}();

        boolean bool = ${tableInfo.lowerCamelCase}Service.add${tableInfo.upperCamelCase}(${tableInfo.lowerCamelCase});
        if (bool) {
            System.out.println("新增${tableInfo.simpleRemark}成功");
        } else {
            System.out.println("新增${tableInfo.simpleRemark}失败");
        }

        Instant end = Instant.now();
        System.out.println("代码执行消耗时间: " + Duration.between(begin, end).toMillis() + " 毫秒");
    }

    /** 测试修改${tableInfo.simpleRemark} */
    @Test
    public void testUpdate${tableInfo.upperCamelCase}() {
        Instant begin = Instant.now();

        ${tableInfo.upperCamelCase} ${tableInfo.lowerCamelCase} = ${tableInfo.lowerCamelCase}Service.get${tableInfo.upperCamelCase}ById("1241271018726662146");
        if (${tableInfo.lowerCamelCase} == null) {
            System.out.println("${tableInfo.simpleRemark}不存在");
            return;
        }
        boolean bool = ${tableInfo.lowerCamelCase}Service.update${tableInfo.upperCamelCase}(${tableInfo.lowerCamelCase});
        if (bool) {
            System.out.println("修改${tableInfo.simpleRemark}成功");
        } else {
            System.out.println("修改${tableInfo.simpleRemark}失败");
        }

        Instant end = Instant.now();
        System.out.println("代码执行消耗时间: " + Duration.between(begin, end).toMillis() + " 毫秒");
    }

    /** 测试根据主键ID删除${tableInfo.simpleRemark} */
    @Test
    public void testDelete${tableInfo.upperCamelCase}ById() {
        Instant begin = Instant.now();

        boolean bool = ${tableInfo.lowerCamelCase}Service.delete${tableInfo.upperCamelCase}ById("1241271018726662146");
        if (bool) {
            System.out.println("删除${tableInfo.simpleRemark}成功");
        } else {
            System.out.println("删除${tableInfo.simpleRemark}失败");
        }

        Instant end = Instant.now();
        System.out.println("代码执行消耗时间: " + Duration.between(begin, end).toMillis() + " 毫秒");
    }

    /** 测试根据主键ID集合批量删除${tableInfo.simpleRemark} */
    @Test
    public void testDelete${tableInfo.upperCamelCase}ByIds() {
        Instant begin = Instant.now();

        List<String> idList = new ArrayList<>();

        boolean bool = ${tableInfo.lowerCamelCase}Service.delete${tableInfo.upperCamelCase}ByIds(idList);
        if (bool) {
            System.out.println("批量删除${tableInfo.simpleRemark}成功");
        } else {
            System.out.println("批量删除${tableInfo.simpleRemark}失败");
        }

        Instant end = Instant.now();
        System.out.println("代码执行消耗时间: " + Duration.between(begin, end).toMillis() + " 毫秒");
    }
}